
## Getting Started

Install by running:  
``` yarn ```

To execute the simple test run:  
``` yarn start ```

### Notes

* App has an example in `start.js` with pointing at express server. Recommend changing to localhost. 
* Uses pupeteer in front of chrome, headless can be toggled via the `headless` boolean in `start.js`
* This is a very basic demo. Directory structure and execution setup is for example only.