module.exports = async function resizeWindow(page, browser, width, height) {
  await page.setViewport({ width, height });

  // Window frame - probably OS and WM dependent.
  // height += 105;

  // Any tab.
  const {
    targetInfos: [{ targetId }],
  } = await browser._connection.send('Target.getTargets');

  // Tab window.
  const { windowId } = await browser._connection.send(
    'Browser.getWindowForTarget',
    { targetId },
  );

  // Resize.
  await browser._connection.send('Browser.setWindowBounds', {
    bounds: { height, width },
    windowId,
  });
};
