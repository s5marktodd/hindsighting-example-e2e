const delay = require('./delay');

async function clickSelector(page, selector) {
  await delay(100);
  await page.waitForSelector(selector);
  return await page.click(selector);
}

module.exports = clickSelector;
