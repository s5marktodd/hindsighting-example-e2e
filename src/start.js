const puppeteer = require('puppeteer');
const resizeWindow = require('./utils/resizeWindow');
const configs = require('../testconfig.json');

const {
  login,
  selectApp,
  setScope,
  ensureCollectionLoaded,
} = require('./actions/access');
const { gotoSummaryView, gotoCategoryRecap } = require('./actions/pagenavs');

const url = configs.url || 'http://localhost:8080';
const size = [1800, 900];
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
  });
  const page = await browser.newPage();
  (async () => {
    await resizeWindow(page, browser, ...size);

    await page.goto(`${url}/login`);
    await login(page);
    await selectApp(page);
    await setScope(page);
    await ensureCollectionLoaded(page);
    await gotoSummaryView(page);

    await gotoCategoryRecap(page);

    await page.screenshot({
      path: 'screenshot.png',
    });
    // browser.close();
  })().catch(e => {
    console.error(e);
    browser.close();
  });
})();
