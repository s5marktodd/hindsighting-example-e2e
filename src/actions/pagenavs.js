const clickSelector = require('../utils/clickSelector');

async function gotoSummaryView(page) {
  await clickSelector(page, `[data-qa=summaryview-link`);
  return await page.waitForSelector(
    `[data-qa-component=StandardCardView] [data-qa-component=Overlay][hidden]`,
  );
}

async function gotoCategoryRecap(page) {
  await clickSelector(page, `[data-qa=categoryrecap-caret]`)
  await clickSelector(page, `[data-qa=categorysummary-link]`)
  return await page.waitForSelector(
    `.category-summary [data-qa-component=Overlay][hidden]`,
  );
}

module.exports = {
  gotoSummaryView,
  gotoCategoryRecap
};
