const clickSelector = require('../utils/clickSelector');
const delay = require('../utils/delay');

async function login(page) {
  console.info('Attempting login.');

  await page.waitForSelector('.auth0-lock-submit');
  await page.type('input[name=email]', 'vishal.shah@s5stratos.com');
  await page.type('input[name=password]', '123');

  await page.click('button.auth0-lock-submit');
  console.info('Successfully logged in.');
}

async function selectApp(page) {
  console.info('Attempting to select app.');

  const selector = '[data-id=bottomup]';
  await clickSelector(page, selector);

  console.info('Successfully selected app.');
}

async function setScope(page) {
  console.info('Selecting scope.');

  await page.waitForSelector('[data-qa-component="Overlay"][hidden]'); // wait for overlay to go away
  const dep = {
    btn: '[data-qa=dropdown-productmember]',
    member: '[title="RETAIL W DRESSY WOVEN TOPS"]',
  };
  const grp = {
    btn: '[data-qa=dropdown-locationmember]',
    member: '[title="TOTAL EXPRESS"]',
  };

  const dates = {
    btn: `[data-qa=datepicker-assortmentperiod]`,
    startDate: `[aria-label="Tue May 08 2018"]`,
    endDate: `[aria-label="Mon May 14 2018"]`,
  };

  const submit = `[data-qa=button-scopeselectorsubmit]`;
  await clickSelector(page, dep.btn);
  await clickSelector(page, dep.member);

  await delay(300);

  await clickSelector(page, grp.btn);
  await clickSelector(page, grp.member);

  await delay(300);

  await clickSelector(page, dates.btn);
  await clickSelector(page, dates.startDate);
  await clickSelector(page, dates.endDate);

  await clickSelector(page, submit);

  await page.waitForSelector('[data-qa=TopNav');
  console.info('Selected a scope ya jabroni.');
}

async function ensureCollectionLoaded(page) {
  await page.waitForSelector(`[data-qa=headerContainer]`);
}

module.exports = {
  login,
  selectApp,
  setScope,
  ensureCollectionLoaded,
};
